package thread;

import chatroom.ChatRoom;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ServerThread extends Thread {

    private Socket clientSocket;
    private static ArrayList<ChatRoom> chatroomList = new ArrayList<>();
    private ChatRoom chatroomChosen;

    // Name of the server log file
    private static final String LOG_FILE_NAME = "serverlog.log";

    // Key word to close the program in chat client
    private static final String EXIT_KEY = "/quit";

    // Key word to close the current chatroom
    private static final String BACK_KEY = "/back";

    // Program options that the server uses to interact with the client
    private static final int OPTION_CHAT = 1;
    private static final int OPTION_SHOW_CHATS = 2;
    private static final int OPTION_CREATE_CHATROOM = 3;
    private static final int OPTION_JOIN_CHAT = 4;
    private static final int OPTION_HELP = 5;
    private static final int OPTION_EXIT = 6;
    private static final int OPTION_LEAVE_CHATROOM = 7;

    public ServerThread(Socket socket) {

        clientSocket = socket;
        writeInServerFileLog("Client connected from: "
                + clientSocket.getLocalAddress() + " " + clientSocket.getLocalPort());
    }

    @Override
    public void run() {

        String message = "";
        int option = 0;
        JSONParser parser = new JSONParser();
        boolean isRunning = false;

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            do {

                String line = (br.readLine());
                writeInServerFileLog("Option chosen " + line);

                JSONObject messageParsed = (JSONObject) parser.parse(line);

                /* If the message contains "option", then I store the option to execute one of the options 
                that the server has to interact with the client*/
                if (messageParsed.containsKey("option")) {
                    option = Integer.parseInt(messageParsed.get("option").toString());
                    isRunning = isExecutingOptions(option, messageParsed, message, clientSocket, bw);
                }

            } while (isRunning);

        } catch (NullPointerException ex) {
            System.out.println("The line received, is " + ex.getMessage() + " the client might forced the exit");
            writeInServerFileLog("The line received, is " + ex.getMessage() + " the client might forced the exit");

        } catch (IOException ex) {
            System.err.println("CONNECTION ERROR: " + ex.getMessage());
            writeInServerFileLog("CONNECTION ERROR: " + ex.getMessage());

        } catch (ParseException ex) {
            System.err.println("Error while parsing JSON in server thread " + ex.getMessage());
            writeInServerFileLog("Error while parsing JSON in server thread " + ex.getMessage());

        } finally {

            try {

                writeInServerFileLog("CONNECTION CLOSED");

                if (chatroomChosen.deleteClientWhenExit(clientSocket)) {

                    writeInServerFileLog("The client " + clientSocket.getLocalAddress() + " .. deleting from list");
                    deleteChatRoom();
                }

                clientSocket.close();

            } catch (IOException ex) {
                writeInServerFileLog("IOException error in server on: " + ex.getMessage());
                System.err.println("IOException error in server on: " + ex.getMessage());

            } catch (NullPointerException ex) {
                writeInServerFileLog("The chatroom chosen is " + ex.getMessage());
                System.err.println("The chatroom chosen is " + ex.getMessage());
            }
        }
    }

    /**
     * Test method that creates three default chatrooms in server
     */
    public synchronized static void createDefaultChatrooms() {

        chatroomList.add(new ChatRoom("Chunk_kids", "Minecraft"));
        chatroomList.add(new ChatRoom("White cat", "Animals & fauna"));
        chatroomList.add(new ChatRoom("Java programmers", "Code"));
    }

    /**
     * Depending of the option sended by client, the server will perform a
     * different action.
     *
     * @param option
     * @param messageParsed
     * @param message
     * @param clientSocket
     * @param bw
     * @throws IOException
     */
    private boolean isExecutingOptions(int option, JSONObject messageParsed, String message, Socket clientSocket, BufferedWriter bw) throws IOException {

        boolean isRunning = true;

        switch (option) {

            case OPTION_CHAT:

                // Gets the messages sended by the clients
                if (messageParsed.containsKey("messages")) {
                    message = messageParsed.get("messages").toString();

                    if (chatroomChosen != null) {
                        System.out.println(message);
                        chatroomChosen.broadcastMessagesToClients(message, clientSocket);
                    }
                }
                break;

            case OPTION_SHOW_CHATS:

                showChatrooms(bw);
                break;

            case OPTION_CREATE_CHATROOM:

                String name = null;
                String thematic = null;
                boolean error = true;

                // Gets the name and thematic sended by the client.
                if (messageParsed.containsKey("name") && messageParsed.containsKey("thematic")) {
                    
                    name = messageParsed.get("name").toString();
                    thematic = messageParsed.get("thematic").toString();

                    if (createChatroom(name, thematic)) {

                        chatroomChosen = chatroomList.get(chatroomList.size() - 1);
                        joinClientToChat(chatroomList.size() - 1, bw);
                        error = false;
                    }

                }

                /* In case there is any error while adding the client to the 
                chatroom created, this message will be sended*/
                if (error) {

                    writeInServerFileLog("An error ocurred while adding to chatroom, please try again");

                    JSONObject chatroomError = new JSONObject();
                    chatroomError.put("error", "An error ocurred while adding to chatroom, please try again");
                    writeInBuffer(chatroomError.toJSONString(), bw);
                }

                break;

            case OPTION_JOIN_CHAT:

                int position = 0;

                // Get the position sended by the client, and it is parsed to Integer number.
                if (messageParsed.containsKey("chatpositioninlist")) {

                    position = Integer.parseInt(messageParsed.get("chatpositioninlist").toString());
                    joinClientToChat(position, bw);
                }
                break;

            case OPTION_HELP:

                writeInBuffer(showHelp(), bw);
                break;

            case OPTION_EXIT:

                writeInServerFileLog("The client " + clientSocket.getLocalAddress() + " requested exit");

                if (chatroomChosen != null) {

                    if (chatroomChosen.deleteClientWhenExit(clientSocket)) {

                        writeInServerFileLog("The client " + clientSocket.getLocalAddress() + " was deleted from the list");
                        deleteChatRoom();

                    } else {

                        System.err.println("The client was not deleted from the list");
                        writeInServerFileLog("The client was not deleted from the list");
                    }

                    isRunning = false;
                }

                break;

            case OPTION_LEAVE_CHATROOM:

                if (chatroomChosen != null) {

                    writeInServerFileLog("The client " + clientSocket.getLocalAddress() + " was deleted from the list");
                    chatroomChosen.deleteClientWhenExit(clientSocket);

                    deleteChatRoom();
                    chatroomChosen = null;
                }
                break;

            default:

                writeInServerFileLog("Incorrect option");
        }

        return isRunning;
    }

    /**
     * Join the client to the requested chatroom.
     *
     * @param position
     * @param bw
     * @throws IOException
     */
    private void joinClientToChat(int position, BufferedWriter bw) throws IOException {

        JSONObject clientAdded = new JSONObject();

        if (chatroomList.size() > position && position > -1) {

            chatroomChosen = chatroomList.get(position);

            if (chatroomChosen != null) {

                JSONObject clientInChat = new JSONObject();
                clientInChat.put("isClientAdded", true);
                clientInChat.put("messages", "Now you're in chatroom " + chatroomChosen.getName() + " <" + chatroomChosen.getThematic() + ">"
                        + System.lineSeparator() + "Other clients connected: " + chatroomChosen.getClientsInChatRoomList().size());
                clientAdded.put("clientAdded", clientInChat);

                // Add the client socket to the requested chatroom.
                addToSelectedChatroom(chatroomChosen, clientSocket);
            }

        } else {

            JSONObject clientInChat = new JSONObject();
            clientInChat.put("isClientAdded", false);
            clientInChat.put("messages", "The chatroom you tried to join, doesn't exist, please, try again");
            clientAdded.put("clientAdded", clientInChat);

        }

        writeInBuffer(clientAdded.toJSONString(), bw);
    }

    public synchronized boolean createChatroom(String name, String thematic) {

        return chatroomList.add(new ChatRoom(name, thematic));
    }

    /**
     * Add the client to the requested chatroom.
     *
     * @param chatroomChosen
     * @param clientSocket
     */
    private void addToSelectedChatroom(ChatRoom chatroomChosen, Socket clientSocket) {

        if (chatroomChosen.addClientToList(clientSocket)) {

            writeInServerFileLog("Client " + clientSocket.getLocalAddress() + " added to list of chatroom " + chatroomChosen.getName());

        } else {

            System.out.println("The client " + clientSocket.getLocalAddress() + " wasn´t added to list");
            writeInServerFileLog("The client " + clientSocket.getLocalAddress() + " wasn´t added to list");
        }
    }

    private String showHelp() {

        return ("To close the program while chatting, type " + EXIT_KEY + " or " + EXIT_KEY.toUpperCase()
                + System.lineSeparator() + "To return to main menu, type "
                + BACK_KEY + " or " + BACK_KEY.toUpperCase());
    }

    /**
     * Sends the available chatrooms in the server.
     *
     * @param bw
     *
     */
    private synchronized void showChatrooms(BufferedWriter bw) {

        try {

            writeInBuffer(parseChatRoomsToJSON(), bw);

        } catch (IOException ex) {

            System.err.println("Error while showing available chatrooms " + ex.getMessage());
            writeInServerFileLog("Error while showing available chatrooms " + ex.getMessage());
        }
    }

    /**
     * Put the different JSON Objects into a JSON Father object. If there is
     * none, the json father will has a message warning of it.
     *
     * @return 
     *
     */
    private synchronized String parseChatRoomsToJSON() {

        JSONObject jsonFather = new JSONObject();
        JSONArray chatroomsArray = new JSONArray();

        for (ChatRoom chatRoom : chatroomList) {

            JSONObject jsonSon = new JSONObject();
            jsonSon.put("availablechats", System.lineSeparator() + "AVAILABLE CHATROOMS" + System.lineSeparator());
            jsonSon.put("name", chatroomList.indexOf(chatRoom) + "--> " + chatRoom.getName());
            jsonSon.put("thematic", "<" + chatRoom.getThematic() + ">");
            jsonSon.put("numbofpeople", "clients connected: " + chatRoom.getClientsInChatRoomList().size());
            chatroomsArray.add(jsonSon);
        }

        if (chatroomsArray.isEmpty()) {

            jsonFather.put("emptyChatrooms", System.lineSeparator() + "There are no chatrooms created yet" + System.lineSeparator());
        }

        jsonFather.put("chatrooms", chatroomsArray);
        return jsonFather.toJSONString();
    }

    private synchronized void deleteChatRoom() {

        if (isChatroomChosenEmpty()) {

            writeInServerFileLog("Deleting chatroom " + chatroomChosen.getName() + " because it is empty...");
            chatroomList.remove(chatroomChosen);
        }
    }

    private boolean isChatroomChosenEmpty() {

        return chatroomChosen.getClientsInChatRoomList().isEmpty();
    }

    /**
     * Writes the string passed by parameter in the specified buffered writer.
     *
     * @param stringToWrite
     * @throws IOException
     * @param bw
     *
     */
    private void writeInBuffer(String stringToWrite, BufferedWriter bw) throws IOException {

        bw.write(stringToWrite);
        bw.newLine();
        bw.flush();
    }

    /**
     * Creates the log file if it not exists in the specified path.
     *
     * @return
     *
     */
    private synchronized void writeInServerFileLog(String content) {

        try (BufferedWriter bufferedWriterLogFile = Files.newBufferedWriter(createLogFile(), StandardOpenOption.WRITE,
                StandardOpenOption.APPEND);) {

            writeInBuffer(LocalDateTime.now() + " " + content, bufferedWriterLogFile);

        } catch (IOException ex) {

            System.err.println("Error while writing in server file log " + ex.getMessage());
        }
    }

    /**
     * Creates the log file if it not exists in the specified path.
     *
     * @return
     *
     */
    private Path createLogFile() throws IOException {

        File file = new File(LOG_FILE_NAME);
        Path filePath = file.toPath();

        if (!Files.exists(filePath)) {

            Files.createFile(filePath);
        }

        return filePath;
    }
}
