package chatserver;

import java.io.IOException;
import java.net.ServerSocket;
import thread.ServerThread;

/**
 *
 * @author silvia
 */
public class ChatServer {

    private static final int DEFAULT_PORT = 4444;

    public static void main(String[] args) {

        try {
            int port = DEFAULT_PORT;

            if (args.length > 0 && !args[0].isEmpty()) {

                port = Integer.parseInt(args[0]);
            }

            runServer(port);

        } catch (NumberFormatException ex) {
            System.err.println("The port of the server must be numeric " + ex.getMessage());

        } catch (IOException ex) {
            System.err.println("Error while launching server.." + ex.getMessage());
        }
    }

    /**
     * Creates new and starts server thread for every client connections.
     * @param port
     */
    public static void runServer(int port) throws IOException {

        try {

            System.out.println("Server started.. waiting for connections..."
                    + System.lineSeparator() + "Listening on port: " + port);

            ServerSocket serverSocket = new ServerSocket(port);

            // The server is constantly listening for newer pettitions, so this will never stop.
            while (true) {
                new ServerThread(serverSocket.accept()).start();
                System.out.println("new thread started...");
            }

        } catch (NumberFormatException ex) {

            System.err.println("Error, the port must be a numeric " + ex.getMessage());
        }
    }
}
