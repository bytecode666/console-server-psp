package chatroom;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;

public class ChatRoom {

    private String name;
    private String thematic;
    private ArrayList<Socket> clientsInChatRoomList;
    private ArrayList<String> sendedMessagesByClients;
    private static final int QUANTITY_OF_MESSAGES_TO_SEND = 10;
    private static final int NUMBER_TO_DIVIDE = 2;

    public ChatRoom(String name, String thematic) {

        clientsInChatRoomList = new ArrayList<>();
        sendedMessagesByClients = new ArrayList<>();

        this.name = null;
        if (!name.isEmpty()) {
            this.name = name;
        }

        this.thematic = null;
        if (!thematic.isEmpty()) {
            this.thematic = thematic;
        }
    }

    /*Method that add the client to the list, and writes the list of messages
    in its output. This is done in the JSON format.*/
    public synchronized boolean addClientToList(Socket clientSocket) {

        try {

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            try {

                String listMessages = "{\"messages\": [";

                for (String message : sendedMessagesByClients) {
                    listMessages += message + ", ";
                }

                listMessages += "]}";
                bw.write(listMessages);
                bw.newLine();
                bw.flush();

            } catch (IOException ex) {
                System.err.println("IOException sendind last messages because of  " + ex.getMessage());
            }
        } catch (IOException ex) {
            System.err.println("IOException in addClientToList because of " + ex.getMessage());
        }

        return clientsInChatRoomList.add(clientSocket);
    }

    // Deletes the client from list
    public synchronized boolean deleteClientWhenExit(Socket clientLeaving) {
        return this.clientsInChatRoomList.remove(clientLeaving);
    }

    /* Method that sends the messages to the other clients that are in the same
    chatroom.*/
    public void broadcastMessagesToClients(String message, Socket sender) throws IOException {

        BufferedWriter bw;

        /*When the history of messages is full (size=10), the position that is the result dividing the size per 2
        to give space to the arraylist. 
        Important to synchronize this, to avoid race conditions.*/
        synchronized (this) {

            if (sendedMessagesByClients.size() >= QUANTITY_OF_MESSAGES_TO_SEND) {
                sendedMessagesByClients.remove(sendedMessagesByClients.size() / NUMBER_TO_DIVIDE);
            }

            sendedMessagesByClients.add(message);

            /*The messages are sent in the JSON way. This will be better, 
            because it is sended compressed.*/
            String listMessages = "{\"messages\": [";
            for (String messageInList : sendedMessagesByClients) {
                listMessages += messageInList + ", ";
            }
            listMessages += "]}";

            /*This writes in every socket output that are in the list the list
            of messages sended by clients.*/
            for (Socket socket : clientsInChatRoomList) {

                bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                bw.write(listMessages);
                bw.newLine();
                bw.flush();

            }
        }

        bw = null;
    }

    // Getter methods
    public ArrayList<Socket> getClientsInChatRoomList() {
        return this.clientsInChatRoomList;
    }

    public String getThematic() {
        return this.thematic;
    }

    public String getName() {
        return this.name;
    }
}